# -*- coding: utf-8 -*-
#from sikuli import *
from time import sleep
import random

myScriptPath = ".\\"
addImportPath(myScriptPath)
import images,step,util

# ★★★★★ 個人化設定 ★★★★★★
# 打幾場 0-2 修一次，假設坦克是滿血下去打的，自己斟酌。
tank_tank_time = 5
# -------------------------------------------

#############################################
# 固定參數，可能因 UI改版而變動。

# counter = 0 固定 不要異動(計算腳本執行次數)，放在這是為了讓別的function 可以取得
counter = 0

# 0-2 的中間機場 至 上方的的偏移比率。
C1 = [-0.0087, -0.407]
#############################################

def test(R):
    # 設定成 True 就會 直接執行 此區 code，用於測試自己的function 時。
    if False:
        util.log_debug('TEST=GO')

        step.three_In_ONE(R)
        
        util.log_debug("TEST=END")
        util.log_debug("wait...")
        wait(100)
    pass
#######################################################
#                                                                                                                                #
#    注意: 必須把 練等隊伍在 T1, 打手T2 , T3空白,且 T2 等待補給                # 
#          事先選擇 第0戰役，並且 進 0-2 調到 最大視野 後退出 回到 基地     #
#                                                                                                                                #
#######################################################

def main():
    util.log_debug( "============MAIN() BEGIN============")
    #--------------------------------------------確保可以從主畫面進到(作戰任務、後勤支援、模擬作戰... 那邊)
    step.from_home_to_combat(R)
    #--------------------------------------------選擇0-2 and "看到" 普通作戰 但不會按下去
    step.click_regular_battle_0_2(R)
    #------------------------------------------- 普通作戰  按下去
    if   "CAMP_HAS_BEEN_FULL" ==  step.click_regular_battle_goto_map(R) :
        util.log_debug("[main] : 點擊人形強化....") 
        util.click_it_until_vanish(R,images.dollsPowerUp,6)
        #-------------------------------------------  上方會確定進去工廠，下方就全力完成工廠的事情就好，
        #-------------------------------------------  可重複使用的FUNC，想要 「三個願望」，呼叫他就對了。
        step.three_In_ONE(R)
        #------------------------------------------- 回到 基地主畫面在這邊重新做一次 main()~
        util.click_it_until_vanish(R,images.RTB,6)
        main()
        #-------------------------------------------直接返回 不用再做 if 以外的 code，因為已經打完 main()了
        return 
    #------------------------------------------- 
    util.log_debug("[main]:Good to Go!") 
    #------------------------------------------- 先準備好 等等要用的 區域，因為人形部屬後會看不到完整的 圖示。
    
    while True:
        # 記憶 指揮部 的位置    
        main_Base_region = step.get_MiniBase_Region(R)
        # 記憶 機場 的位置
        airport_region = step.get_MiniAirport_Region(R)
        if ( airport_region is not None) and (main_Base_region is not None):
            break
        util.log_debug("[retry][main]:嘗試重新記憶 機場、指揮部位置!") 
    # End while
    util.log_debug("[info][main]:已獲得 base_region, airport_region!") 
    #-------------------------------------------點下headquarters(指揮部)，按下確定~
    # global 表示 這邊的 兩個參數是用最上方 的全域變數
    global counter
    global tank_tank_time
    step.click_Main_Base_byRegion(R,main_Base_region)
    if (counter%(tank_tank_time)) is 0 :
        util.log_debug("[main]: 已執行 "+ str(counter) +" 次, \'需要\'維修 !!!")
        # 此維修 很有可能會 lag，注意下一步是否會除錯。
        step.fix_mostRightDoll_inDeploy(R)
    else:
        util.log_debug("[main]: 已執行 "+ str(counter) +" 次, 不需維修 !!!")
    #------------------------------------------- 按下 隊伍編成 @retry_yes
    step.click_team_edit(R)
    #-------------------------------------------
    # 在隊伍編成畫面呼叫，交換打手。
    step.change_mostLeftfighter_OneTwo(R) 
    #-------------------------------------------
    # 按下白色返回
    step.click_RTB_white(R) 
    #-------------------------------------------
    # 按下指揮部
    step.click_Main_Base_byRegion(R,main_Base_region)
    #-------------------------------------------
    # 部署梯隊
    step.deploy_team(R)
    #-------------------------------------------按下機場，按下確定~
    step.click_0_2_airport_byRegion(R,airport_region)
    step.deploy_team(R)    
    #-------------------------------------------  開始作戰
    step.click_List_battle_start(R) 
    #-------------------------------------------  補給 第二梯隊伍
    util.randClickRegion(airport_region) # 點第二梯隊
    # 按下 airport_region，直到 images.supply 被看到，如沒出現等待3秒再次操作
    util.click_it_until_appear(R,airport_region,images.supply,3)
    step.click_supply(R)# 按下補給        
    #------------------------------------------- 選擇第一梯隊
    while not step.echelon_X_isSelected(R,1):# 如果T隊1 沒有被選到
        util.randClickRegion(main_Base_region) # 點第一梯隊。
    #------------------------------------------- 讓白色的計畫模式變成黃色的計畫模式
    step.click_plan_mode_onFocus(R)
    #------------------------------------------- 取得 白點、敵人指揮部的 Region 
    white_point_region = step.get_0_2_WhitePoint_Region(R)
    enemy_base_region = step.get_0_2_enemyBase_Region (R)
    #------------------------------------------- 因為已經進入計畫模式 依序點這 兩點 即可
    #------------------------------------------- 點白點
    util.log_debug("[main]:點白點...")
    util.click_it_until_appear(R,white_point_region,images.movement_point_1,4) 
    #------------------------------------------- 點 敵人指揮部
    util.log_debug("[main]:敵人指揮部...")
    util.click_it_until_appear(R,enemy_base_region,images.movement_point_negative_2,4)
    #------------------------------------------- 執行計畫
    util.log_debug("[main]:執行計畫...")
    util.click_it_until_vanish(R,images.plan_exec,6)
    #-------------------------------------------  等到回合結束 持續存在10秒後
    util.log_debug("等待160秒...")
    wait(160)
    if False:
        util.wait_PS_KeepExistsInTime(R,images.round_end,10)
        #------------------------------------------- 已經打完 三隻，點數為0，按下結束回合,有可能失敗。
        util.randClick(R,images.round_end,6)
        #------------------------------------------- 等 行動點數 等三秒
        util.log_debug("[main]:等待 行動點數 03.... 存在 6 秒")
        util.wait_PS_KeepExistsInTime(R,images.zero_three,6)
        #------------------------------------------- 讓白色的計畫模式變成黃色的計畫模式 @retry_yes
        util.log_debug("[main]:點下白色的計畫模式")
        step.click_plan_mode_onFocus(R)
        #------------------------------------------- 得到第一梯隊的位置，利用下方的紅色機場 做偏移(此偏移量上面已經訂好了)。
        global C1
        enemy_airport = util.getRegionByImages(R,images.red_airport)
        offset_from_enemy_airport_region = util.getShiftRegion_ShowIt(R,enemy_airport,C1[0],C1[1])
        offset_from_enemy_airport_region.highlight(2,"#F1F1F1")
        #------------------------------------------- # 選擇 第二梯隊
        # click_it_until_appear(# 遊戲畫面,
                                                    # 要按的目標,
                                                    # 按到這個圖案(選擇梯隊時會出現的黃色準心) 出現為止,
                                                    # 檢查週期為 5 秒,
        util.click_it_until_appear(  R,
                                                    offset_from_enemy_airport_region,
                                                    images.lock_line_0_2,
                                                    5)
    
        #------------------------------------------- 選擇敵方指揮部
        # 遊戲畫面, (R)
        # 要按的目標, (敵人的指揮部)
        # 按的圖案出現為止, (行動點數為零)
        # 檢查週期為 5 秒,
        util.click_it_until_appear(  R,
                                                        images.enemy_base_0_2,
                                                        Pattern(images.one_point).similar(0.8),
                                                        5)
        #------------------------------------------- 執行計畫
        util.click_it_until_vanish(R,images.plan_exec,6)
        util.log_debug("[main]: 等待30秒")
        wait(30)
    # End if
    #------------------------------------------- 等待彈藥耗盡 持續出現 6秒
    util.wait_PS_KeepExistsInTime(R,images.bullet_exhaust,6)
    #------------------------------------------- 按下結束回合 
    util.click_it_until_vanish(R,images.round_end,6)
    #------------------------------------------- 等待  結算畫面的 返回基地 持續存在 3秒
    util.wait_PS_KeepExistsInTime(R,images.RTB_transparency_0_2,3)
    #------------------------------------------- 按下  結算畫面的 返回基地
    util.log_debug("[main]: 按下  結算畫面的 返回基地")        
    util.click_it_until_vanish(R,images.RTB_transparency_0_2,2)
    #------------------------------------------- 跑動畫用的  等待
    util.log_debug("[main]: 等待 5秒...")        
    wait(5)
    #------------------------------------------- 可能出現你撈到人形 or 沒有。跳過那個分享畫面。
    util.log_debug("[main]: 檢查 分享圖案 是否存在? 檢查 3 次...")         
    for i in range(3):
        util.log_debug("[retry][main]: 重新檢查 分享圖案 是否存在?")        
        if util.getRegionByImages(R,images.share_icon) is None:
            wait(1)
            util.log_debug("[retry][main]: 第 " + str(i+1) + " 次 找不到")   
            offsetRegion_of_share_icon = None
        else:
            util.log_debug("[nice][main]: 已找到!!")         
            offsetRegion_of_share_icon = util.getRegionByImages(R,images.share_icon).offset(105,60)
            if offsetRegion_of_share_icon is not None:
                break
    #------------------------------------------- 按下 分享 的旁邊
    while True:
        if offsetRegion_of_share_icon is not None:
            if R.exists(images.share_icon,2):
                util.randClickRegion(offsetRegion_of_share_icon)
                if R.exists(images.RTB_transparency_0_2,3):
                    break
                else:
                    continue
            else:
                util.log_debug("[ERROR][main]: 分享圖標又不見了??")         
        else:
            break
    #------------------------------------------- 等待  結算畫面的 返回基地 持續存在 2秒
    util.wait_PS_KeepExistsInTime(R,images.RTB_transparency_0_2,2)
    #------------------------------------------- 按下  結算畫面的 返回基地
    util.click_it_until_vanish(R,images.RTB_transparency_0_2,2)
    #------------------------------------------- 計數器++，決定下輪是否要維修
    counter +=1 
    util.log_debug( "=========================MAIN() E N D========================="  )
    return
##-----------------------------------------------
def clear_log():
    # 執行一次
    # 清空檔案
    #fp=open('./log.txt','w')
    #fp.close()
    pass
#####################################
#Obsever function
def ifFindPause(event):
    print "Ober: 偵測到暫停"
    if exists(images.battle_play,3):
        exists(images.battle_play,3)
        util.randClick(R, images.battle_play, 3)
        print "Ober: rand click play btn"
        wait(2)
    print "Ober: return"
    event.repeat()
def logisticsObserver(event):
    print "Ober: 偵測到後勤歸來"
    #後勤支
    util.randClick(R, images.logistics_str,10)
    print "Ober: rand click 後勤支"
    # 後勤的 確定
    util.randClick(R, images.logistics_checked, 10)
    print "Ober: rand click 確定 return"    
    event.repeat()
def systemInfo(event):
    pass
    while exists(Pattern("system info.png").targetOffset(-197,-21),6):
        print"Ober:找到 系統公告,->找尋 關閉"
    while exists("close in factory.png",15):
        print"Obse:找到 關閉"
    while exists(Pattern("activity_info.png").targetOffset(-148,-13),15):
        print"Obse:找到 活動"
    event.repeat()
def shikikanProfileObserver(event):
    print "Obser:點擊到指揮官資料..."
    pass
    event.repeat()
# 直接去 按後勤支援的區域就好了，因為下面已經有定義好了，直接改來用~
def achievementObserver(event):
    print "Obser:獲得成就..."
    R_logistics = step.getLogisticsRegion(R)
    R_logistics.setW(int(R_logistics.getW()*0.75))
    util.randClickRegion(R_logistics)
    event.repeat()
def interuptObserver(event):
    print "Obser:中斷計畫?"
    pass
    event.repeat()

#####################################
if __name__ == '__main__':
    #呼叫util 內的 init() ，做一些環境參數的修改。
    util.init()
    
    # 設定一些 Region 。R  = 少前遊戲視窗
    R = selectRegion() # 跳出 拉選視窗
    
    # 決定是否開啟 baretail，由util 上方的 AUTO_OPEN_LOG_TRACER 決定!!
    util.init_baretail()
    ##  <get_Observer_Region>
    # 取得 頂部(戰鬥暫停、繼續) 的region
    R_header = step.getHeaderRegion(R) 
    R_header.highlight("#edad0e") 
    
    # 取得 "後勤支" 字樣 會發生的 region
    R_logistics = step.getLogisticsRegion(R)  
    R_logistics.highlight("#edad0e")

    # 取得  獲得成就 字樣 會發生的區域
    R_get_achievement = step.get_achievement_4WORD_region(R)
    R_get_achievement.highlight("#edad0e")
    ##  </get_Observer_Region>

    ## toggle off
    wait(1)
    R_header.highlight()
    R_logistics.highlight()
    R_get_achievement.highlight()
    
    ##  <register_Observer>
    # 註冊 觀察事件。然後就開始觀察了。
    R_header.onAppear(Pattern(images.battle_play).similar(0.92), ifFindPause)#全體撤退 觸發時
    R_header.observeInBackground(FOREVER)
    
    #觀察 "後勤支" 字樣
    R_logistics.onAppear(images.logistics_str, logisticsObserver)
    R_logistics.observeInBackground(FOREVER)

    #觀察 "獲得成就" 字樣 
    R_get_achievement.onAppear(images.get_achievement,achievementObserver)
    R_get_achievement.observeInBackground(FOREVER)
    ##  </register_Observer>

    # ----------------------
    # 清空 log ，該檔案在 util.sikuli 設定 log_FILENAME(檔案名稱)
    clear_log()
    # 測試區塊，if 條件打開就可以跑自己的測試code
    test(R)    
    # ----------------------
    while True :
        #計時開始
        tStart = time.time()
        # ----------------------
        #去打0-2
        main()

        # ----------------------
        #計時結束
        tEnd = time.time()
        global counter
        # 開啟檔案，紀錄執行次數用的
        fp=open('./time_log.txt','a')       
        msg = time.strftime("%a %H:%M:%S", time.localtime()) + ', Execute time: ' + str(counter) + '\n' 
        costTime  = "It cost " + str(tEnd - tStart) + 'sec' + '\n====================\n'
        fp.write(msg)
        fp.write(costTime)
        fp.close()
