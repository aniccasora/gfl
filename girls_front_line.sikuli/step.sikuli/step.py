# -*- coding: UTF-8 -*-
from sikuli import *
import random

myScriptPath = ".\\"
addImportPath(myScriptPath)
import images,util


#############################################
# 固定參數，可能因 UI改版而變動。

# 比率參數 = [x, y, w, h]
# 工廠  選擇角色時 最左上角人形區域的比率。
fac1_list =[ 0.00259291, 0.136294, 0.131374, 0.320061 ]
# 工廠  人形強化 tag進去，左方 「選擇角色」(旁邊白中間黃色加號) 區域的比率。
fac2_list =[ 0.176318, 0.179173, 0.134831, 0.437979 ]
# 工廠 -> 回收拆解 三個加號，最左邊那個加號(選擇角色)的 區域比率
fac3_list =[ 0.197061, 0.18683, 0.153846, 0.171516 ]
# 工廠 -> 回收拆解 連續三個加號 的區域比率
fac4_list =[ 0.197061, 0.18683, 0.469317, 0.171516 ]
# 部署梯隊畫面 ， 最右邊的人形 的區域比率。
dep1_list = [ 0.698358, 0.211332, 0.133967, 0.399694 ]

# 戰鬥時 -> 暫停戰鬥、繼續戰鬥 的區域比率
bat1_list = [ 0.404494, 0, 0.182368, 0.0765697 ]
# 部署梯隊時 -> 最"左"邊人形 的區域比率
bat2_list =[ 0.119274, 0.212864, 0.13051, 0.396631 ]

# 主畫面 -> 後勤 歸來字樣的 區域比率
home1_list = [ 0.00432152, 0.212864, 0.33535, 0.396631 ]
# 主畫面 獲得成就，的字樣 可能出現區域的 區域比率 X
home2_list = [0.293, 0.147, 0.318, 0.339]

# 2019/09/02
# 將地圖縮至"最小"，其0-2指揮部的Region 比率
miniBase_region_ratio=[ 0.492653, 0.479326, 0.0250648, 0.0398162 ]
# 將地圖縮至"最小"，其0-2機場的Region 比率
miniAirport_region_ratio = [ 0.343993, 0.473201, 0.0181504, 0.0336907 ]
# 將地圖縮至"最小"，"y座標最大"(平面座標系)的"白點"，Region比率
__0_2_white_point_ratio = [ 0.501296, 0.133231, 0.0172861, 0.0290965 ]
# 將地圖縮至"最小"，0-2敵方指揮部的Region 比率
__0_2_enemyBase_ratio = [ 0.621435, 0.105666, 0.0242005, 0.0229709 ]
#############################################


# 拖動時所用的參數
# [滑鼠按下之前的等待秒數, (左鍵已經按下)拖動前等待秒數, 滑鼠拖到目標後等多久放開, 拖移到目標 所花費時間]
for_0_2_DragDrop_PARAM = [0.3, 1, 0.3, 4]

# lv1 圖片的 相似度設定，因為這個數值有時候很不固定所以拉上來當全域變數改 
# 圖片比對預覽的 測試 結果填在這邊。
lv1_icon_similar = 0.6

# 統一延遲變數，step 的 function，依賴下面變數，不要改名不然會抓不到
# 自己斟酌調整，畢竟多等個幾秒可以大大增加穩定性。
# 這是全域變數。小心調整。
DELAY_TIME = 3
DELAY_TIME_LONG = 6
ONE_SEC = 1
click_it_until_vanish_DELAYTIME = 3


# 如有要按某個按鈕但不知道有有沒有寫過
# 請 Ctrl+F -> 輸入 "click_XXXXXX" (XXXXXX : images.sikuli 圖片變數全名)

# step 檔裡面的每一個 function幾乎都要給 R(egion) 參數，因為要"限定區域"。

# 從主畫面按下戰鬥，確保可以成功後才會結束
def from_home_to_combat(R):
    waitTime = 1
    # R 表示 少前畫面區域。
    while not R.exists(images.Combat_Black_icon, waitTime):
        util.log_debug( "001.找不到 黑色戰鬥圖標(images.Combat_Black_icon)")
        waitTime = 6
        # 如果存在戰鬥圖示
        if R.exists(images.Combat_Yellow_Icon, waitTime):
            util.log_debug( "002.存在 黃色戰鬥圖標(images.Combat_Yellow_Icon)")
            #點選戰鬥圖示
            util.randClick(R,images.Combat_Yellow_Icon,waitTime)
            util.log_debug( "003.rand click 黃色戰鬥圖標(images.Combat_Yellow_Icon)")
            # 如存在 返回基地按鈕
            if R.exists(images.RTB,25):
                util.log_debug( "004.存在 返回基地 離開迴圈")
                break
    util.log_debug( "1~4.存在 返回基地 開始選擇0-2")
    util.log_debug("[step]:from_home_to_combat()--complete!")
#--------------------------------------------
# 進去 0-2。點擊"普通作戰"按鈕
def click_regular_battle_0_2(R):
    while not R.exists(images.Regular_Battle, 2):
        util.log_debug( "005.不存在 黃色普通作戰 圖標(images.Regular_Battle)")
        util.randClick(R,images.Black_Massage,20)
        util.log_debug( "006.rand click 0-2黑色情報(images.Black_Massage)")
    util.log_debug("5~6.發現 普通作戰 已離開迴圈")
    util.log_debug("[step]:click_regular_battle_0_2()--complete!")
#--------------------------------------------
# 點下去 普通作戰,並確保可以到地圖。
# 如果跳出軍營上限，則回到main，不在這邊呼叫強化、賣糧、拆核心。
def click_regular_battle_goto_map(R):
    while R.exists(images.Regular_Battle, 3):
        util.log_debug("007.嘗試點擊 普通作戰....")
        util.randClick(R,images.Regular_Battle, 3)
        util.log_debug("[step]: 等待 2 秒...")
        wait(2)
        util.log_debug("008.檢查 是否有 人形強化?")
        if R.exists(images.dollsPowerUp,2):
            util.log_debug("[WARN][step]: 注意 倉庫已滿，接下來去 工廠...")
            return "CAMP_HAS_BEEN_FULL"
        #----------
        util.log_debug("[retry]007.檢查 是否有 普通作戰...")    
    util.log_debug( "[step]:click_regular_battle_goto_map()--complete!")
#-------------------------------------------- 

# 取得  機場 的 region  for 0-2, 其他地圖請自己增加。
def getAirport_Region(R):
    return util.getRegionByImages(R,images.List_airport_0_2)
#--------------------------------------------
# 點下headquarters(指揮部)
# @reuse:其他地圖如果也有指揮部，就可以呼叫function，不須用重寫。
def click_Main_Base_byRegion(R, headquarters_region):
    funcName = "click_Main_Base()"
    util.log_debug( "[step]:"+funcName+"--start~")
    waitTime = 3 
    util.click_it_until_appear(R,headquarters_region,images.choice_team,waitTime)
    util.log_debug( "[step]"+funcName+"--complete!")
#--------------------------------------------
# 按下部屬梯隊的"確定"。
# @reuse:可搭配 click_Main_Base()，達成部屬梯隊的功能。
def deploy_team(R):
    funcName = "deploy_team()"
    util.log_debug( "[step]"+funcName+"--start!")
    waitTime = 6
    util.click_it_until_vanish(R,images.battle_select_confirm,waitTime)
    util.log_debug( "[step]"+funcName+"--complete!")
#-------------------------------------------- 
# 按下 0-2 的機場
def click_0_2_airport_byRegion(R,airportRegion):
    waitTime = 1
    util.log_debug("016.尋找0-2 機場!~~")
    waitTime = 3 
    util.click_it_until_appear(R,airportRegion,images.choice_team,waitTime)
    util.log_debug("[step]:click_0_2_airport()--complete!")
#--------------------------------------------
# 按下 "隊伍編成" 的按鈕。 在梯隊部署時會在左下角看到。
# @reuse:可重複使用
def click_team_edit(R):
    funcName = "click_team_edit()"
    util.log_debug("[step]:"+funcName+"--start!")
    
    if exists(images.choice_team):
        util.log_debug("[step]:"+funcName+"--Good to Go~!")
    else:
        util.log_debug("[WARN][step]:"+funcName+"--NOT ready! 隊伍編成根本不在此畫面")
        return
    
    waitTime = 1
    while not exists(images.form_default, waitTime):
        waitTime = 6
        util.log_debug("Ex1.已經進入 選擇梯隊，點選 \'隊伍編成\'!!")
        util.randClick(R,images.team_edit,DELAY_TIME)
    util.log_debug("[step]:"+funcName+"--complete!")
    
#--------------------------------------------
# 點擊你要的T隊，拿images 的圖來吃
# @reuse:可重複使用。
# @param: R(Region)。遊戲畫面。
# @param: wantTeamNumberPS(PS)。你要的梯隊 圖片。
# @param: waitTime(int)。有預設數值，呼叫時可以不用填，也可以自行決定你要的秒數
def click_echelon(R,wantTeamNumberPS,waitTime = click_it_until_vanish_DELAYTIME):
    funcName = "click_echelon()"
    util.log_debug("[step]:"+funcName+"--start!")
    util.click_it_until_vanish(R,wantTeamNumberPS,waitTime)
    util.log_debug("[step]:"+funcName+"--complete!")
#--------------------------------------------
# 讓T1 <---> T2 ，最左邊的打手交換~~，在部署梯隊時呼叫才有用。
# 尋找 的方式是 一開始 就記住  1、2隊長的位置，因為位置不會因為你移出梯隊就改動
# (如果之後改版有異動這種特性，這邊的邏輯就會出錯)
# 2019/09/02因應改版，只要換一隻人形，另外一對的使用中人形就會替換，變得很方便。
# @reuse
def change_mostLeftfighter_OneTwo(R):
    funcName = "change_mostLeftfighter_OneTwo()"
    util.log_debug("[step]:"+funcName+"--start!")
    if exists(images.form_default,DELAY_TIME):
        util.log_debug("018.已經進入 梯隊編成 介面，框出第一打手!!")
        # 編輯兩個梯隊時都會用到  mostLeftFighterRegion
        mostLeftFighterRegion = getMostLeftDollRegion(R)
        click_echelon(R,Pattern(images.echelon_1_blur).similar(0.85))# 按下第一梯隊
        waitTime = 1
        while not exists(images.remove_from_echelon,waitTime):
            util.log_debug("019.選擇第一打手!!")
            waitTime = 6
            util.randClickRegion(mostLeftFighterRegion)
            util.log_debug("020.等待 "+ str(DELAY_TIME) + " 秒...")
            wait(DELAY_TIME)
            util.log_debug("021.[try_again]檢查是否有 \'移出梯隊\' ~~")
        util.log_debug("022. \'移出梯隊\'  已檢出!!")
        util.log_debug("023. 記憶 隊長2 位置。")       
        leader2_region = util.getRegionByImages(R,images.lead_two)
        if  leader2_region is None:
            util.log_debug("[ERROR][step]"+funcName+":記憶 隊長2 dismatch!!") 
            return False

        #--------    
        waitTime = 1
        while not exists(images.form_default,waitTime):
            waitTime = 6
            util.log_debug("024.選擇隊長 2 !")
            util.randClickRegion(leader2_region)
        util.log_debug("025.隊長 2 已經入隊-- complete!")
    #---End IF   
    else:
        util.log_debug("[ERROR]:"+funcName+"--尚未進入選擇梯隊介面!")
        return    
    util.log_debug("[step]:"+funcName+"--complete!")
#-------------------------------------------- 
#--------------------------------------------
# 點選白色的返回基地。
# @reuse
def click_RTB_white(R,waitTime = click_it_until_vanish_DELAYTIME):
    funcName = "click_RTB_white()"
    util.log_debug("[step]:"+funcName+"--start!")
    util.log_debug("Ex1.嘗試點選 \'白色的\' 返回基地!")
    util.click_it_until_vanish(R,images.RTB_white,waitTime)
    util.log_debug("[step]:"+funcName+"--complete!")
#--------------------------------------------
# 點選黃色的返回基地
# @reuse
def click_RTB(R,waitTime = click_it_until_vanish_DELAYTIME):
    funcName = "click_RTB()"
    util.log_debug("[step]:"+funcName+"--start!")
    util.log_debug("Ex1.嘗試點選 \'黃色的\' 返回基地!")
    util.click_it_until_vanish(R,images.RTB,waitTime)
    util.log_debug("[step]:"+funcName+"--complete!")
#--------------------------------------------
# 按下 戰鬥開始
# 依賴 images.內的圖片List。
def click_List_battle_start(R):
    funcName = "click_List_battle_start()"
    util.log_debug("[step]:"+funcName+"--start!")
    waitTime = 1
    util.log_debug("032.尋找 開始作戰...")
    while not R.exists(images.round_end, waitTime):
        waitTime = 6
        util.log_debug("033.點擊 開始作戰!")
        util.randClick(R,images.List_battle_start,ONE_SEC)
        util.log_debug("034.[try_again]檢查是否點到 開始作戰...")
    util.log_debug("[step]:"+funcName+"--complete!")
#--------------------------------------------
# 在 隊伍編成 取得第一位(最左邊)人形 的 region。
def getMostLeftDollRegion(mainRegion):
    # 使用最上方定義的參數...
    global bat2_list
    while util.getRelativeSubRegion(mainRegion, bat2_list) is None:
        util.log_debug("[retry][step]: 取得第一位(最左邊)人形 的 region失敗，等待1秒...")
        wait(1)
    res_region = util.getRelativeSubRegion(mainRegion, bat2_list)
    return res_region
#--------------------------------------------
# 獲得最右邊角色的 Region，在部屬梯隊的畫面呼叫時可以看的到。
# @return : res_region(Region)。
def get_mostRightDoll_Region_inDeploy(mainRegion):    
    funcName = "get_mostRightDoll_Region_inDeploy()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 依賴最上定義的變數
    global dep1_list
    while util.getRelativeSubRegion(mainRegion, dep1_list) is None:
        util.log_debug("[WARN][step]: 最右邊角色的 Region取得失敗，重新獲取...")
        wait(1)
    res_region = util.getRelativeSubRegion(mainRegion, dep1_list)
    # <註解>
    if False:
        res_region =Region(\
                int(mainRegion.w*0.71+mainRegion.x),\　
                int(mainRegion.h*0.24+mainRegion.y),\
                int(mainRegion.w*0.113),\
                int(mainRegion.h*0.34)) 
    # </註解>
    util.log_debug("[step]:"+funcName+"--complete!")
    return res_region 
#--------------------------------------------
# 幫最右邊的角色 按下維修 
def fix_mostRightDoll_inDeploy(R):
    funcName = "fix_mostRightDoll_inDeploy()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 取得最右邊角色的 Region
    needFixDollRegion = get_mostRightDoll_Region_inDeploy(R)
    # 取得 維修畫面的 確定按鈕 85% 相似度
    chkBtn_pattern85 = Pattern(images.deploy_fix_check).similar(0.85)
    # 先點一次試試，點進去也沒關係
    util.randClickRegion(needFixDollRegion)
    if R.exists(images.NOT_NEED_FIXED,1):
        util.log_debug("[WARN][step]: 不須維修...") 
        return
    util.log_debug("[step]: 嘗試點擊 最右邊的坦克...") 
    waitTime = 6
    util.click_it_until_appear(R,needFixDollRegion,chkBtn_pattern85,waitTime)
    util.log_debug("[step]: 嘗試點擊 快速維修 的確定...")
    util.click_it_until_vanish(R,chkBtn_pattern85,6)
    util.log_debug("[step]: 已點擊 確定，回到部署畫面!!") 
    util.log_debug("[step]:"+funcName+"--complete!")
#--------------------------------------------
# 專門為 0-2 往上拖動設計的 function，土炮寫法，不要讓圖片找不到，他會噴錯誤。
def for_0_2_DragDrop(R):
    funcName = "for_0_2_DragDrop()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 最上面定義的全域變數
    global for_0_2_DragDrop_PARAM
    # 滑鼠按下之前的等待秒數
    Settings.DelayBeforeMouseDown = for_0_2_DragDrop_PARAM[0]
    # 拖動前 等待秒數
    Settings.DelayBeforeDrag = for_0_2_DragDrop_PARAM[1]
    # 滑鼠拖到 目標後 等多久放開?
    Settings.DelayBeforeDrop = for_0_2_DragDrop_PARAM[2]
    # 目標 所花費時間，過程才是重點，不會LAG就短一些，不過還是穩點。
    Settings.MoveMouseDelay = for_0_2_DragDrop_PARAM[3]
    #-------------------------------
    util.log_debug("[step]:開滑準備  3秒前...")
    wait(1)
    util.log_debug("[step]:開滑準備  2秒前...")
    wait(1)
    util.log_debug("[step]:開滑準備  1秒前...")
    wait(1)
    util.log_debug("[step]:開滑!!")
    #  "white_circle_0_2.png"  ----拖到這邊---->>  "round_end.png"
    R.dragDrop(images.white_circle_0_2,images.round_end)
    util.log_debug("[step]:拖放完畢!!")
    # 已經滑完 把改的參數 條回來
    Settings.DelayBeforeMouseDown = 0.3
    Settings.DelayBeforeDrag = 0.3
    Settings.DelayBeforeDrop = 0.3
    Settings.MoveMouseDelay = 0.25 
    util.log_debug("[step]:"+funcName+"--complete!")
#-------------------------------------------- 
# @return: True or False
# 判斷你有沒有選中 梯隊(echelon)，選中時會有妖精的釋放圖標。
def chk_echelon_selected(R):
    funcName = "chk_echelon_selected()"
    util.log_debug("[step]:"+funcName+"--start!")
    if  R.exists(images.fairy_release,3):
        util.log_debug("[step]:"+funcName+"--complete!")
        R.getLastMatch().highlight(1)
        return True
    else:
        util.log_debug("[step]:"+funcName+"--Failed!")
        return False
#-------------------------------------------- 
# 按下白色的計畫模式  -----直到-----> 出現 黃色的計畫模式 為止
def click_plan_mode_onFocus(R):
    funcName = "click_plan_mode_onFocus()"
    util.click_it_until_vanish(R,images.plan_mode_blur,6)
    util.log_debug("[step]:"+funcName+"--complete!")
#-------------------------------------------- 
# 按下 補給而已，waitTime 呼叫時 選填，預設 是最上面給的 變數值
def click_supply(R,waitTime = click_it_until_vanish_DELAYTIME):
    util.click_it_until_vanish(R,images.supply,waitTime)
####    確定  梯對 X 有被選到，by 妖精的圖案
def echelon_X_isSelected(R,teamNumber):
    funcName = "echelon_X_isSelected()"
    util.log_debug("[step]:"+funcName+"--start!")
    fairyImg = getEchelonX_UsedFairy_imgName(images.echelon_fairy,teamNumber)# 依賴 : images 的 echelon_fairy
    util.log_debug("第 "+str(teamNumber)+" 梯隊, 為 \'"+ fairyImg +"\' 妖精")
    util.log_debug("開始尋找.....delay time = "+ str(DELAY_TIME)) 
    if R.exists(fairyImg,DELAY_TIME):
        util.log_debug("[step]:"+funcName+"--complete!")        
        return True
    else:
        util.log_debug("[step]:"+funcName+"--Failed!")
        return False
# 回傳 第 N 梯隊用的 妖精 圖片名稱
# @reuse
def getEchelonX_UsedFairy_imgName(fairyList,number):
    if number<1 or number >10 :
        util.log_debug( "[ERROR]Fuck")
        return
    if len(fairyList) < number:
        util.log_debug( "[ERROR]Fuck")
        return
    return fairyList[number-1]
############################################
# 三個願望一次滿足，到工廠以後，幫你 強化角色 -> 多的狗糧賣掉 -> 拆核心 -> END
def three_In_ONE(R):
    funcName = "三個願望[強化、賣狗糧、拆核心]一次滿足!!!()"
    util.log_debug("[step]:"+funcName+"--start!")
    util.log_debug("[step]: 正在檢查是否在工廠...")
    # <畫面是否在 工廠 檢查開始>
    factory_80_Pattern = Pattern(images.factory_icon).similar(0.8)
    util.wait_PS_KeepExistsInTime(R,factory_80_Pattern,5)
    # </畫面是否在 工廠 檢查完畢>
    util.log_debug("[step]: 確定在工廠--complete")    
    #-------------------------------------- 選擇人形強化 標籤，按下白色的那個
    util.click_it_until_vanish(R,Pattern(images.dollsPowerUp_factory_blur).similar(0.85),3)
    #-------------------------------------- 先獲得  + 號 區域， 欲強化角色 的那個格子。
    doll_selected_grid = get_wantPowerUpDoll_empty_Region_inFactory(R)
    #-------------------------------------- 選擇狗糧 -> 按下強化 -> 選擇 欲強化角色  loop..
    while True:
        waitTime = 1
        while not R.exists (images.select_filter,waitTime):
            waitTime=4
            util.log_debug("[step]: 嘗試按下 取得的  選擇角色Region...")    
            wait(1)
            util.randClickRegion(doll_selected_grid)
            util.log_debug("[retry][step]: 檢查 角色 過濾元件是否存在...")    
            wait(1)        
        util.log_debug("[retry][step]: 存在!!")        
        #--------------------------------------  這邊 的 人形列表，有時lag，有時不lag，真的就只能等久一點了。
        mostFeftTop_dollRegion = get_mostLeftTopDoll_Region_inFactory(R)
        # 80% 相似度的 背景，最左上角人形的背景，有人形可選擇時他是被擋住的。
        PTN_NBK_80=Pattern(images.null_background_in_select_Window).similar(0.8)
        util.log_debug("[step]:  判斷是否有角色可以選擇.... 判斷 7 秒...")         
        if False is R.waitVanish(PTN_NBK_80,7):
            util.log_debug("[step]:  等了 7秒 沒有角色。 去賣狗糧了~")
            util.click_it_until_vanish(R,Pattern(images.cancel_to_select).similar(0.77),5)        
            #--------------------------------------  去賣狗糧、拆核心
            retire_dolls(R)
            #--------------------------------------  結束
            util.log_debug("[step]:"+funcName+"--Early_complete! RETURN")
            #----------------- 沒拉
            util.log_debug("[step]: 三個願望一次滿足() --complete!!") 
            return
        #--------------------------------------  有角色可以強化時
        util.log_debug("[nice][step]:  有可強化角色!!，點他... ")
        util.click_it_until_appear(R,mostFeftTop_dollRegion,images.dollsPowerUp_factory_focus,6)
        #--------------------------------------  選狗糧(按下加號)，有可能會沒有狗糧時，沒有狗糧要去，拆核心。★★★★
        util.log_debug("[nice][step]:  選中 欲強化人形,開始選狗糧...(按下加號)")
        util.click_it_until_vanish(R,images.plus_to_select_Dog_food,5)
        #-------------------------------------- 接下來分三種路，
        #  一、(無人形or無2星) 
        #  二、(只有3星、欲強化人形已經滿數值按下智能選擇不會改變)
        #  三、(2星、3星 都有 )
        #-------------------------------------- 一、(無人形or無2星) 判斷
        util.log_debug("[step]:  判斷是否有角色可以選擇.... 判斷 7 秒...")         
        if False is R.waitVanish(PTN_NBK_80,7):
            util.log_debug("[step]:  等了 7秒 沒有 任何人形。 直接返回 至工廠.... RETURN")
            util.click_it_until_vanish(R,Pattern(images.cancel_to_select).similar(0.77),5)
            #----------------- 
            util.log_debug("[step]: 沒有任何人形可以 當狗糧(也無核心)，直接返回.... RETURN")  
            util.log_debug("[step]: 三個願望一次滿足() --complete!!") 
            return
        #--------------------------------------- 二、(只有3星or欲強化人形已經滿數值按下智能選擇不會改變) 判斷
        util.log_debug("[step]: 如果存在 智能選擇，嘗試按他 ，並觀察是否消失?")
        for i in range(3):
            util.log_debug("[step]: 第 "+ str(i+1)+ "次!") 
            if R.exists(images.smart_select,2):
                util.randClick(R,images.smart_select,6)
                wait(1)
            else:
                util.log_debug("[step]: 智能選擇 已不存在...")
                break
        #--------------</ for End> 
        #-------------------------------------- 
        if R.exists(images.smart_select,2):
            util.log_debug("[WARN][step]:已經按下 智慧選擇 數次，仍存在此按鈕，\n"+
                                "「欲強化人形應該滿數值了」，\n"+\
                                "「或是只剩下 三星以上的人形了」!!")
            wait(1)
            util.log_debug("[step]:  直接返回 至工廠，並且去拆核心....")
            #----------------- 按下取消
            util.click_it_until_vanish(R,Pattern(images.cancel_to_select).similar(0.77),5)    
            #-----------------去拆核心 
            dismantling_FirstPageDolls(R)
            #----------------- 拆完就沒拉
            util.log_debug("[step]: 三個願望一次滿足() --complete!!") 
            return
        else:
            util.log_debug("[step]: 智能選擇 已不存在...[double check!]")
        #------------------------------------------------- 三、(2星、3星 都有 ) 時走這邊
        util.log_debug("[step]: 二三星都有人形!!")
        #-------------------------------------- 按下智能選擇，即使上面有先按過也沒關係。
        util.click_it_until_vanish(R,images.smart_select,5)
        #-------------------------------------- 按下確定(for 狗糧)
        util.click_it_until_vanish(R,images.chkIcon_whenSelected,5)
        #-------------------------------------- 按下強化(for 真的讓狗糧被吃掉) ★★ 強化收益問題
        #util.click_it_until_appear(R,images.powerUpBtn,powerUpCompleteTitle,5)
        waitTime = 1
        while not R.exists(images.close_btn_msg,waitTime):
            waitTime = 1
            util.log_debug("[step]: 嘗試按下強化")
            util.randClick(R,images.powerUpBtn,6)
            util.log_debug("[step]: 檢查是否出現強化收益問題....3秒") 
            if R.exists(images.powerUp_low_efficiency_msg,3):
                util.log_debug("[WARN][step]: 出現強化收益問題，去拆狗糧、拆核心....")
                #------------------ 先按下取消....
                left_half_Region = Region(R.getX(),R.getY(),int(R.getW()/2),R.getH())
                waitTime = 4
                while left_half_Region.exists(images.low_efficiency_msg_cancelBtn,waitTime):
                    util.log_debug("[step]: 嘗試按下 左半邊的 取消....")
                    util.randClick(left_half_Region,images.low_efficiency_msg_cancelBtn,6)
                    util.log_debug("[retry]: 等待 2 秒...")
                    wait(2)
                util.log_debug("[retry]: 又回到工廠了!!!")    
                #------------------ 
                util.log_debug("[step]: 剩下兩個願望...[賣狗糧、拆核心]")
                retire_dolls(R)
                # 直接返回，走到這邊的CASE，已經是沒有狗糧可以強化的狀態。
                return
            if R.exists(images.close_btn_msg,6):
                util.log_debug("[nice][step]: 強化完成!!") 
                break
        #-------------------------------------- 強化訊息出現，按下關閉
        util.log_debug("[nice][step]: 按下關閉!!") 
        util.click_it_until_vanish(R,images.close_btn_msg,5)
        #-------------------------------------- 重新去選擇 欲強化的角色
    #-------------------------------------- 
    util.log_debug("[step]:")
    util.log_debug("[step]:"+funcName+"--complete!")
#--------------------------------------------
# 呼叫時要在，工廠畫面 否則報錯
# 賣掉人形2星 -> 拆核心(3,4,5星) -> 結束
def retire_dolls(R): 
    funcName = "retire_dolls()"
    util.log_debug("[util]:"+funcName+"--start!")
    util.log_debug("[step]: 正在檢查是否在工廠...")
    # <畫面是否在 工廠 檢查開始>
    factory_80_Pattern = Pattern(images.factory_icon).similar(0.8)
    util.wait_PS_KeepExistsInTime(R, factory_80_Pattern, 5)
    # </畫面是否在 工廠 檢查完畢>
    util.log_debug("[step]: 確定在工廠--complete")
    #-------------------------------------- 選擇回收拆解 標籤，按下白色的那個
    util.log_debug("[step]: 嘗試按下 白色的 回收拆解 按鈕...")
    wait(1)
    util.click_it_until_vanish(R, Pattern(
        images.doll_retire_blur).similar(0.85), 3)
    util.log_debug("[step]: 回收拆解  標籤 已按下--complete")
    #-------------------------------------- 去選擇狗糧...
    #  ------- 這邊就不用 util.click_it_util... 系列的 function了
    #  ------- 這邊的判斷很 麻煩
    #  ------- 可能根本沒有 人形可以拆 ★
    #-------------------------------------- 獲得最左邊 + 號，Region
    while get_mostLeftDoll_emptyGrid_Region_inFactory(R) is None:
        util.log_debug("[WARN][step]: 嘗試重新獲得 選擇角色Region...")
        wait(1)
    mostLeftPlusRegion = get_mostLeftDoll_emptyGrid_Region_inFactory(R)
    #-------------------------------------- 按下最左邊 + 號，Region
    while True: 
        util.log_debug("[WARN][step]: 嘗試按下 選擇角色Region...")
        util.randClickRegion(mostLeftPlusRegion)
        # ------------------ 按下 + 號時，檢查是否有 無人形訊息跳出，如出現就結束此 func
        util.log_debug("[WARN][step]: 觀察最多3秒，是否有警示訊息...")
        if R.exists(images.no_dolls_can_retire_msg,3):
            util.log_debug("[WARN][step]: 無人形可供拆解訊息已被發現 ，不拆解人形!!")
            return 
        if R.exists(images.cancel_to_select,3):
            util.log_debug("[nice][step]: 有人形可供拆解 !") 
            break
    #-------------------------------------- </ while END> 已經確定 進入 選擇欲拆解人形之介面。(有智能選擇)
    #-------------------------------------- 
    util.log_debug("[step]: 如果存在 智能選擇，嘗試按他 ，並觀察是否消失?")
    for i in range(3):
        util.log_debug("[step]: 第 "+ str(i+1)+ "次!") 
        if R.exists(images.smart_select,2):
            util.randClick(R,images.smart_select,6)
            wait(1)
        else:
            util.log_debug("[step]: 智能選擇 已不存在...")
            break
    #--------------</ for End> 
    #-------------- 要按下確定了
    util.log_debug("[step]: 嘗試按下確定....")
    # 遊戲畫面: R
    # 要按下的目標 : 確定
    # 等待出現圖片: 拆解
    # 等待時間: 6秒
    util.click_it_until_appear(R,
                                images.chkIcon_whenSelected,
                                images.dismantling_btn,
                                6)
    #-------------- 要按下拆解了
    # -- 取得 "原本" 三個連續加號的 所在區域
    originPlus_R = get_THREEplus_Region_inFactory(R)
    #-------------------------------------- 按下拆解 直到 人形拆掉為止...
    waitTime = 1
    while not originPlus_R.exists(images.three_continue_plus,waitTime):
        waitTime = 6
        util.log_debug("[step]: 仍存在待分解人形 嘗試按下拆解....")    
        util.randClick(R,images.dismantling_btn,6)
        util.log_debug("[retry][step]: 判斷三個加號是否在預設位置...")
    util.log_debug("[step]: 已經拆完狗糧--complete，接下來拆心...")
    #-------------------------------------- 拆核心...
    dismantling_FirstPageDolls(R)
    #--------------------------------------
    util.log_debug("[util]:"+funcName+"--complete!")
#--------------------------------------------
# 檢查是否在工廠 -> 回收拆解 -> 分解 ("二")三四五星，因只會分解第 1 頁的人形而已，
# 所以如果上方被狗糧(二星)，擋住的話他就不會真的只分解到核心...
# 所這個 function name 取名為 "分解_第一頁人形(R)"
def dismantling_FirstPageDolls(R):
    funcName = "dismantling_FirstPageDolls()"
    util.log_debug("[util]:"+funcName+"--start!")
    util.log_debug("[step]: 正在檢查是否在工廠...")
    # <畫面是否在 工廠 檢查開始>
    factory_80_Pattern = Pattern(images.factory_icon).similar(0.8)
    util.wait_PS_KeepExistsInTime(R, factory_80_Pattern, 5)
    # </畫面是否在 工廠 檢查完畢>
    util.log_debug("[step]: 確定在工廠--complete")
    #-------------------------------------- 選擇回收拆解 標籤，按下白色的那個
    util.log_debug("[step]: 嘗試按下 白色的 回收拆解 按鈕...")
    wait(1)
    util.click_it_until_vanish(R, Pattern(
        images.doll_retire_blur).similar(0.85), 3)
    util.log_debug("[step]: 回收拆解  標籤 已按下--complete")
    #-------------------------------------- 去選擇狗糧...
    #  ------- 這邊就不用 util.click_it_util... 系列的 function了
    #  ------- 這邊的判斷很 麻煩
    #  ------- 可能根本沒有 人形可以拆 ★★★★
    #-------------------------------------- 獲得最左邊 + 號，Region
    while get_mostLeftDoll_emptyGrid_Region_inFactory(R) is None:
        util.log_debug("[WARN][step]: 嘗試重新獲得 選擇角色Region...")
        wait(1)
    mostLeftPlusRegion = get_mostLeftDoll_emptyGrid_Region_inFactory(R)
    #-------------------------------------- 按下最左邊 + 號，Region
    while True: 
        util.log_debug("[WARN][step]: 嘗試按下 選擇角色Region...")
        util.randClickRegion(mostLeftPlusRegion)
        # ------------------ 按下 + 號時，檢查是否有 無人形訊息跳出，如出現就結束此 func
        util.log_debug("[WARN][step]: 觀察最多3秒，是否有警示訊息...")
        if R.exists(images.no_dolls_can_retire_msg,3):
            util.log_debug("[WARN][step]: \'無人形可供拆解\' 訊息已被發現 ，不拆解人形!!")
            return 
        if R.exists(images.cancel_to_select,3):
            util.log_debug("[nice][step]: 有人形可供拆解 !") 
            break
    #-------------------------------------- </ while END> 已經確定 進入 選擇欲拆解人形之介面。(有智能選擇)
    #-------------------------------------- 開始 "手動" 選擇 所有 第一頁面人形
    global lv1_icon_similar
    while R.exists(Pattern(images.lv1_icon).similar(lv1_icon_similar),1) is None:
        util.log_debug("[retry][util]: 嘗試重新找尋 Lv1 圖標...")
        wait(0.5)
    #-------------------------------------- 隨機點擊會用到的參數
    lv1_pic_w = R.exists(Pattern(images.lv1_icon).similar(lv1_icon_similar)).getW()    
    lv1_pic_h = R.exists(Pattern(images.lv1_icon).similar(lv1_icon_similar)).getH()
    #-------------------------------------- 隨意順序按下 lv1 的 圖標
    util.log_debug("[util]: 嘗試 點擊\'所有\' Lv1 圖標...(亮黃色)[FFE700]")
    for lv1_gun in R.findAll(Pattern(images.lv1_icon).similar(lv1_icon_similar)):
        lv1_gun.highlight("#FFE700")
        wait(0.3)
        lv1_gun.highlight()
        X = lv1_gun.getTarget().getX()-(lv1_pic_w/2)
        Y = lv1_gun.getTarget().getY()-(lv1_pic_h/2)
        click(Location( X + random.randint(0, lv1_pic_w),\
                        Y + random.randint(0,lv1_pic_h) ))
    util.log_debug("[util]: \'所有\' Lv1 圖標 已點擊...--complete")
    #--------------------------------------
    util.log_debug("[step]: 嘗試按下確定....")
    # 遊戲畫面: R
    # 要按下的目標 : 確定
    # 等待出現圖片: 拆解
    # 等待時間: 6秒
    util.click_it_until_appear(R,
                                                    images.chkIcon_whenSelected,
                                                    images.dismantling_btn,
                                                    6)
    #-------------------------------------- 按下拆解
    # -- 取得 "原本" 三個連續加號的 所在區域
    originPlus_R = get_THREEplus_Region_inFactory(R)
    #------------------------　按下拆解 直到 人形拆掉為止...
    waitTime = 1
    while not originPlus_R.exists(images.three_continue_plus,waitTime):
        waitTime = 6
        util.log_debug("[step]: 仍存在待分解人形 嘗試按下拆解....")    
        util.randClick(R,images.dismantling_btn,6)
        util.log_debug("[step]: 最長檢查3秒, 檢查 \'注意 高星級 單位...\' 警告訊息 是否出現...")    
        if R.exists(images.attention_msg_title,3):
            util.randClick(R,images.attention_msg_title_chkBtn,6)
        else:
            util.log_debug("[step]: 拆的都是 2 星...")     
        util.log_debug("[retry][step]: 判斷三個加號是否在預設位置...")
    util.log_debug("[util]:   已經拆完核心  "+funcName+"--complete!")
#--------------------------------------------
# 工廠 -> 回收拆解 三個加號，獲得最左邊那個加號(選擇角色)的 subRegion
def get_mostLeftDoll_emptyGrid_Region_inFactory(mainRegion):
    funcName = "get_mostLeftDoll_emptyGrid_Region_inFactory()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 拿 上方定義的全域變數
    global fac3_list
    res_region = util.getRelativeSubRegion(mainRegion, fac3_list)
    util.log_debug("[step]:"+funcName+"--complete!")
    return res_region
#--------------------------------------------
# 工廠  人形強化->選擇腳色進去(選擇要強化的角色那邊) 時，最左上角色(第一位)的 Region，
# 實際得到的Region會較小。
def get_mostLeftTopDoll_Region_inFactory(mainRegion):    
    funcName = "get_mostRightDoll_Region_inDeploy()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 拿 上方定義的全域變數
    global fac1_list
    res_region = util.getRelativeSubRegion(mainRegion,fac1_list)
    util.log_debug("[step]:"+funcName+"--complete!")
    return res_region 
#--------------------------------------------
# 工廠  回收拆解 -> 三個加的區域 (一起)
# 實際得到的Region會較小。
def get_THREEplus_Region_inFactory(mainRegion):    
    funcName = "get_THREEplus_Region_inFactory()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 拿 上方定義的全域變數
    global fac4_list
    res_region = util.getRelativeSubRegion(mainRegion,fac4_list)
    util.log_debug("[step]:"+funcName+"--complete!")
    return res_region 
#--------------------------------------------
# @return 後勤(region)
def getLogisticsRegion(mainRegion):
    if mainRegion is None:
        log_debug("[ERROR]: 請不要在截圖時 終止 sikuli，會導致錯誤，請重新開起專案...")
        return
    global home1_list
    R_header = util.getRelativeSubRegion(mainRegion, home1_list)
    return R_header
#--------------------------------------------
# @return 戰鬥，暫停的按鈕出現區域(region)
def getHeaderRegion(mainRegion):
    if mainRegion is None:
        log_debug("[ERROR]: 請不要在截圖時 終止 sikuli，會導致錯誤，請重新開起專案...")
        return
    global bat1_list
    R_header = util.getRelativeSubRegion(mainRegion, bat1_list)
    #<註解>
    if False:
        R_header = Region(\
            int(mainRegion.w*0.41+ mainRegion.x),\
            int(mainRegion.h*0   + mainRegion.y),\
            int(mainRegion.w*0.186),\
            int(mainRegion.h*0.086))                        
    #</註解>
    return R_header
#--------------------------------------------
# 工廠  人形強化 tag進去，左方 「選擇角色」(旁邊白中間黃色加號) 區域，
# 實際得到的Region會較小。
def get_wantPowerUpDoll_empty_Region_inFactory(mainRegion):    
    funcName = "get_wantPowerUpDoll_empty_Region_inFactory()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 拿 上方定義的全域變數
    global fac2_list
    res_region = util.getRelativeSubRegion(mainRegion,fac2_list)
    util.log_debug("[step]:"+funcName+"--complete!")
    return res_region 
#--------------------------------------------
# 取得 獲得成就的 ， 四個字 可能會出現的 區域
def get_achievement_4WORD_region(R):
    funcName = "get_achievement_4WORD_region()"
    util.log_debug("[step]:"+funcName+"--start!")
    global home2_list
    subRegion = util.getRelativeSubRegion(R,home2_list)
    util.log_debug("[util]:"+funcName+"--complete!")
    return subRegion
##########################################
# 因應改版，地圖可以縮得更小，找image變得非常不可靠，故直接記憶Region 位置。
# 這是一個寫死的FUNCTION，依賴於 "比率"
# @return  0-2指揮部Region
def get_MiniBase_Region (R):
    return util.getRelativeSubRegion(R, miniBase_region_ratio)
##########################################
# 因應改版，地圖可以縮得更小，找image變得非常不可靠，故直接記憶Region 位置。
# 這是一個寫死的FUNCTION，依賴於 "比率"
# @return  0-2機場Region
def get_MiniAirport_Region (R):
    return util.getRelativeSubRegion(R, miniAirport_region_ratio)
##########################################
# 因應改版，地圖可以縮得更小，找image變得非常不可靠，故直接記憶Region 位置。
# 這是一個寫死的FUNCTION，依賴於 "比率"
# @return  0-2機場Region
def get_0_2_WhitePoint_Region (R):
    return util.getRelativeSubRegion(R, __0_2_white_point_ratio)
##########################################
# 因應改版，地圖可以縮得更小，找image變得非常不可靠，故直接記憶Region 位置。
# 這是一個寫死的FUNCTION，依賴於 "比率"
# @return  0-2機場Region
def get_0_2_enemyBase_Region (R):
    return util.getRelativeSubRegion(R, __0_2_enemyBase_ratio)
##########################################

# 如想寫下新的 執行步驟 功能 複製以下 程式模板
def step_example():
    funcName = "step_example()"
    util.log_debug("[step]:"+funcName+"--start!")
    # 寫下你的 code...，想要多複雜就多複雜。
    # 可以取用 util.randClick(....)、util.getRegionByXXX()、...
    #         images.picture_In_Window
    util.log_debug("[util]:"+funcName+"--complete!")
#####
# 範例
def example_function(R):
    # 印出log，此function 定義在外部檔案，"util.silkui" 所以要加上 前置詞"util"
    util.log_debug("印出一些log，最好編排數字(流水號)，"+\
                "Ctrl +  F 可以很快找出錯誤的位置")
    # 你也可以在 util 內部寫 你自訂的 function
    
    # 寫一些 click，你可以用 R.click() 來做你想要點的東西。
    # 剛開始執行時 ， 會要求你 框出一個區域，那就是 region，別名為 'R'。
    # example_function(R) 所傳入的'R'
    R.click("你想要點的.png")
    # 左上方螢幕截圖，點下去可以截圖。(名字不要用中文、空白、詭異符號 只接受底線_。)

    # 當然 也可以 把圖片 放到 images 內統一管理。
    # 這樣就可以 直接呼叫 images.test_picture 了，不怕 實際檔案名稱的變動，還要從程式一個一個改。
    # 
    # <images.sikuli>
    # test_picture = "test.png"
    #</images.sikuli> 

    # 然後像這樣 呼叫你剛剛寫出去的圖片
    R.click(images.test_picture) # 分離式寫法，images 沒有定義 test_picture，這只是範例。
    R.click("test.png")# 寫在一起，按下 Ctrl+T 會有圖片顯示

    # util function 簡易用法
    # 1. 取得 region。
    my_region = util.getRegionByImages(R,images.test_picture)
    # getRegionByImages() 你可以丟list進去，如果怕傳不回region 的話

    # 如果需要高相似度判斷的話 可以呼叫這個，他就會回傳 相似度 大於 87%的 region 回來
    my_region_87 = util.getRegionByPattern(R,Pattern(images.test_picture).similar(0.87))
    #  在my_region隨機點一次
    util.randClickRegion(my_region)
    util.randClickRegion(my_region_87)
     
    # 注意 Region.method，如果有用 R.exists()
    # 呼叫 getLastMatch() 時請加上R   ---->  R.getLastMatch().highlight(1)
    # 單獨 getLastMatch() 會有 None Error。
#
#
#
#####








