# -*- coding: UTF-8 -*-
from sikuli import *

###      <截圖原則>      ###
#
#     這邊的圖片 不要設定任何 相似度。這邊只有  
#     變數名A = "abc.png"
#       或
#     變數名B = ["efg.png"、"ijk.png"]
#
#     不要更動 等號 "左邊"的變數名稱
#
###      </截圖原則>      ###
#
# PS  (PSMRL)

# 計算機的，不用管這張圖
testPic = "testPic.png"

# ★★★★★ 個人化設定 ★★★★★★
# 梯隊 X 用的 妖精，下方有釋放圖示，記得要按"圖片比對預覽"來看一下有沒有抓錯
# 由左到右 1~10，因為只用到 1、2 截兩個就可以了。
#echelon_fairy = ["echelon_1_fairy.png","echelon_2_fairy.png"]
echelon_fairy= ["shield_fairy-1.png", "light_fairy-1.png"]

#--------------------------------------------------


# 　主頁面-> 戰鬥，左上方的戰鬥圖標
Combat_Black_icon = "M_LH_COMBAT_1.png"
# 主頁面 黃色 combat 戰鬥
Combat_Yellow_Icon = "HOME_PAGE_YELLOW_COMBAT_ICON.png"
# 返回基地圖標
RTB = "RETURN_TO_BASE.png"
# 返回 return_white_in_team_edit_interface
RTB_white = "return_white_in_team_edit_interface.png"
# 取消，選擇角色拆解時會看到
cancel_to_select = "select_cancel.png"
# 取消，人形只有少少幾隻狗糧強化時，會出現提示訊息的取消(待解決:會重複標記!!)
low_efficiency_msg_cancelBtn = "low_efficiency_msg_cancelBtn.png"
# 黃色的普通作戰 
Regular_Battle = "Regular_Battle.png"
#0-2 黑色情報
Black_Massage = "Black_Massage.png"
# 選擇梯隊
choice_team = "selet_team.png"
# 指揮部
Main_Base=""
# START
START = "START.png"
# 選擇梯隊時的，確定
battle_select_confirm = "battle_select_confirm.png"
# 0-2 的機場，可以多截幾張，不一樣大小的，
# 這前後的圖片是拿來測試看他會不會找下一個用的，要丟參數時要看func 是否支援 imgList!!
List_airport_0_2 = ""
# 隊伍編成介面的五星圖案。
five_star_SG = "" 
# 戰鬥時-> 繼續作戰(播放鍵)
battle_play = "play.png"
# 後勤支援的圖片 
logistics_str = "logistics.png"
# 後勤支援的 確定 
logistics_checked = "checked.png"
# 拆解 三星以上人形的 確定，基本上一樣，但是預防哪天又改ICON，還是抽開截圖 尚未更新
attention_msg_title_chkBtn = logistics_checked
# 部署時點擊 隨便一個點擊一個人形時會跳出維修畫面的那個確定(注意 需要轉 Pattern，因有兩個確定) 
deploy_fix_check = "deploy_fix_check.png"
# "隊伍編成" 地圖內要部屬梯隊時，會看到。
team_edit = "team_edit.png"
# "梯隊編成"，隊伍編排介面。(這個介面的Dolls 可以拖動)
echelon_info = "echelon_info.png"
# 選擇角色時的 移出梯隊
remove_from_echelon = "remove_from_echelon.png"
# leader_one
lead_one = "lead1.png"
# leader_two
lead_two = "lead_two.png"
# 全空 梯隊
empty_echelon = "null_echelon.png"
# echelon : 梯隊 , blur : 失焦(寫網頁是這樣稱呼的，尚未選中的意思)\
# 這張圖片好好截圖，他會設定到相似度，因為像其他的 梯隊3 會被誤判。
echelon_2_blur = "echelon_2_blur.png"
echelon_1_blur = "echelon_1_blur.png"
# 陣型預設
form_default = "form_default.png"
# 人形強化，當你的 倉庫滿了，進去戰鬥前會跳出 尚未更新
dolls_powerUp = "dolls_powerUp.png"
# 中斷計畫，自動戰鬥時如果按到畫面就會跳出。
whether_interrupt_plan="whether_interrupt_plan-1.png"
# 開始作戰
List_battle_start = ["start1-1.png","start2-1.png","start3-1.png","start4-1.png"]
# 結束回合
round_end = "round_end.png"
# 補給
supply = "supply.png"
#計畫模式 白 *** 小心截圖
plan_mode_blur = "plan_mode_blur.png"
# 計畫模式 黃 *** 小心截圖
plan_mode_focus = "plan_mode_focus.png"
# 0-2 第一回合 應該要點的敵人，行動點數會直接歸零那隻 4795，
# 注意這是唯一一隻站在 普通 紅點的 龍騎，所以可以這樣土炮硬A 已經棄用
round_one_Dragoon4795_in_0_2 =""
# 行動點數為零，計畫模式版本。 尚未更新
zero_point="ZERO_move_point.png"
# 行動點數為一，計畫模式版本。 這個圖片會混淆，要用的時候記得轉 Pattern 設定相似度 尚未更新
one_point = "one_point.png"
# 執行計畫
plan_exec = "plan_exec.png"
# 點數用完 00 尚未更新
zero_zero = "zero_zero.png"
# 行動點數 03 尚未更新
zero_three = "zero_three.png"
# 紅色的機場 尚未更新
red_airport = "red_airport.png"
# 0-2 第二回合，開始時(第一梯隊在 敵方指揮部 左二時，選擇第一梯隊出現的 黃色鎖定條) 尚未更新
lock_line_0_2 = "lock_line_0_2.png"
# 敵方指揮部 0-2 尚未更新
enemy_base_0_2 = "enemy_base_0_2.png"
# 彈藥耗盡
bullet_exhaust = "bullet_exhaust.png"
# 戰鬥結算畫面的返回基地 ,有透明度問題，注意一下，是否會誤判
RTB_transparency_0_2 = "RTB_transparency.png"
# 分享
share_icon = "share_icon.png"
# 按不需要維修的角色會跳出 尚未更新
NOT_NEED_FIXED = "NOT_NEED_FIXED-1.png"
# 人形強化，軍營滿了要進去戰鬥時他會跳出 
dollsPowerUp = "dolls_power_up.png"
# 人形強化，在工廠的圖標，黃色版的，依賴顏色判斷，用90%相似度 去比對，記得轉 pattern
dollsPowerUp_factory_focus = "dollsPowerUp_factory.png"
# 人形強化，在工廠的圖標，白色版的，依賴顏色判斷，用90%相似度 去比對，記得轉 pattern
dollsPowerUp_factory_blur = "dollsPowerUp_factory_blur.png"
# 進去工廠 左上 看到的圖
factory_icon = "factory_icon.png"
# 工廠 選擇角色 強化時 會看到的 篩選項目 記錄點 
select_filter = "select_filter.png"
# 工廠 第一位角色(最左上)，「後方」實驗室的「背景」，沒有角色可選擇時就看到了。 pattern(80% )
null_background_in_select_Window = "null_background_in_select_Window.png"
# 大大的加號 
plus_to_select_Dog_food = "plus_to_select_Dog_food.png"
# 狗糧 選擇時的確定 
chkIcon_whenSelected = "chkIcon_whenSelected.png"
# 智能選擇 
smart_select = "smart_select.png"
# 工廠人形，強化按鈕 
powerUpBtn = "powerUpBtn.png"
# 強化完成訊息 
powerUpCompleteTitle = "powerUpCompleteTitle.png"
# 強化完成訊息 的關閉 
close_btn_msg = "close_btn_msg.png"
# 工廠 回收拆解 黃色 
doll_retire_focus = "doll_retire_focus.png"
# 工廠 回收拆解 白色 
doll_retire_blur = "doll_retire_blur.png"
# 沒角色拆時的訊息
no_dolls_can_retire_msg = "no_dolls_can_retire_msg.png"
# 拆解按鈕 
dismantling_btn = "dismantling_btn.png"
# 三個連續加號 
three_continue_plus = "THREE_PLUS.png"
# 等級一 的圖標。★★★★★ 注意 請進去看 圖片比對預覽，並調"低"，其相似度，
#  但又不會辨識到人形以外的地方!!!。 重要因為等等要條 參數 不是在這邊條，先記下來就好
# 回到 step 上方有個變數 "lv1_icon_similar"，改到那即可 
lv1_icon = "lv1_icon.png"
# 注意，拆解3星以上人形會跳出的提示 
attention_msg_title = "attention_msg_title.png"
#  強化腳色收益問題，餵一隻狗糧時會跳出 
powerUp_low_efficiency_msg = "powerUp_low_efficiency_msg.png"
# 獲得成就，四個字尚未更新
get_achievement = "get_achievement.png"
# 行動點數 已經更新
movement_point_1 = "movement_point_1.png"
movement_point_negative_2 ="movement_point_negative_2.png"
